// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyB7xS8CD6pej8RXcrWz2vdU9KgVOH0PfoI",
  authDomain: "smartcar-connct.firebaseapp.com",
  projectId: "smartcar-connct",
  storageBucket: "smartcar-connct.appspot.com",
  messagingSenderId: "220687664969",
  appId: "1:220687664969:web:272f271816d453ab643c98",
  measurementId: "G-7EF5FB03BB",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth();
const firestore = getFirestore(app);
const storage = getStorage(app);

export { auth, firestore, storage };
