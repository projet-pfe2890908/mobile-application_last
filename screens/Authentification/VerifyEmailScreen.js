import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, StyleSheet } from 'react-native';
import { sendEmailVerification } from 'firebase/auth';
import { auth } from '../../firebaseConfig';

const VerifyEmailScreen = ({ navigation }) => {
  // State variables
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [emailSent, setEmailSent] = useState(false);

  useEffect(() => {
    const unsubscribe = auth.onIdTokenChanged(async (user) => {
        if (user) {
            await user.reload();
            if (user.emailVerified) {
                navigation.navigate('Login'); // Redirect to Login if email is verified
            } else if (navigation.isFocused()) { // Check if the screen is focused before redirecting
                navigation.navigate('VerifyEmail'); // Redirect to VerifyEmailScreen if email is not verified
            }
        }
    });

    return () => {
        unsubscribe();
    };
}, [navigation]);

  // Function to resend verification email
  const handleResendVerificationEmail = async () => {
    try {
      setLoading(true);
      const user = auth.currentUser;
      if (user && !user.emailVerified) {
        await sendEmailVerification(user);
        setEmailSent(true);
        setError(null);
      } else {
        throw new Error('User not found or email already verified');
      }
    } catch (error) {
      console.error('Failed to resend verification email:', error.message);
      setError('Failed to resend verification email. Please try again later.');
    } finally {
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Verify Your Email</Text>
      {error && <Text style={styles.errorText}>{error}</Text>}
      {emailSent && (
        <Text style={styles.successText}>Verification email resent successfully! Please check your inbox.</Text>
      )}
      <Text style={styles.message}>
        A verification email has been sent to your email address. Please check your inbox and follow the instructions to verify your email.
      </Text>
      <TouchableOpacity
        style={[styles.button, loading && styles.disabledButton]}
        onPress={handleResendVerificationEmail}
        disabled={loading || emailSent}
        testID="resendButton"
      >
        {loading ? (
          <ActivityIndicator color="#ffffff" />
        ) : (
          <Text style={styles.buttonText}>Resend Verification Email</Text>
        )}
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Login')}>
        <Text style={styles.loginText}>Back to Login</Text>
      </TouchableOpacity>
    </View>
  );
};

// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  message: {
    marginBottom: 20,
    textAlign: 'center',
  },
  button: {
    width: '100%',
    height: 50,
    backgroundColor: '#007bff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 20,
  },
  disabledButton: {
    backgroundColor: '#ccc',
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  errorText: {
    color: 'red',
    fontSize: 16,
    marginBottom: 10,
  },
  successText: {
    color: 'green',
    fontSize: 16,
    marginBottom: 10,
  },
  loginText: {
    marginTop: 20,
    color: '#007bff',
    textDecorationLine: 'underline',
  },
});

export default VerifyEmailScreen;
