import React, { useState } from 'react';
import { View, ScrollView, Text, TextInput, TouchableOpacity, ActivityIndicator, StyleSheet, RefreshControl } from 'react-native';
import { createUserWithEmailAndPassword, sendEmailVerification } from 'firebase/auth';
import { auth, firestore } from '../../firebaseConfig';
import { doc, setDoc } from 'firebase/firestore';
import { MaterialIcons } from '@expo/vector-icons'; // Import MaterialIcons from Expo vector icons

const RegisterScreen = ({ navigation }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [registrationSuccess, setRegistrationSuccess] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);
    const [refreshing, setRefreshing] = useState(false);


    const handleRegister = async () => {
        try {
            setLoading(true);
            if (!email.trim() || !password.trim() || !confirmPassword.trim()) {
                throw new Error('Please fill in all fields.');
            }
            if (password !== confirmPassword) {
                throw new Error('Passwords do not match.');
            }
            if (password.length < 6) {
                throw new Error('Password should be at least 6 characters long.');
            }
    
            const userCredential = await createUserWithEmailAndPassword(auth, email, password);
            await sendEmailVerification(userCredential.user);
    
            setRegistrationSuccess(true);
            setError(null);
    
            const userRef = doc(firestore, 'users', userCredential.user.uid);
            const userData = {
                email: userCredential.user.email,
                setupCompleted: false,
            };
            await setDoc(userRef, userData);
    
            navigation.navigate('Login');
        } catch (error) {
            console.error('Registration error:', error);
            if (error.code) {
                switch (error.code) {
                    case 'auth/email-already-in-use':
                        setError('Email address is already in use.');
                        break;
                    case 'auth/invalid-email':
                        setError('Invalid email address.');
                        break;
                    case 'auth/weak-password':
                        setError('Password is too weak. It should be at least 6 characters long.');
                        break;
                    default:
                        setError('Registration failed. Please try again later.');
                }
            } else {
                setError(error.message);
            }
        } finally {
            setLoading(false);
        }
    };
    
    const resetState = () => {
        setEmail('');
        setPassword('');
        setConfirmPassword('');
        setLoading(false);
        setError(null);
        setRegistrationSuccess(false);
    };

    const onRefresh = () => {
        setRefreshing(true);
        resetState(); // Reset state when refreshing
        setRefreshing(false);
    };

    return (
        <ScrollView
            contentContainerStyle={styles.container}
            refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
        >
            <View style={styles.content}>
                <Text style={styles.title}>Register</Text>
                {error && <Text style={styles.errorText}>{error}</Text>}
                {registrationSuccess && <Text style={styles.successText}>Registration successful. Please verify your email to login.</Text>}
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    onChangeText={setEmail}
                    value={email}
                    keyboardType="email-address"
                    autoCapitalize="none"
                />
                <View style={styles.passwordInputContainer}>
                    <TextInput
                        style={styles.passwordInput}
                        placeholder="Password"
                        onChangeText={setPassword}
                        value={password}
                        secureTextEntry={!showPassword}
                    />
                    <TouchableOpacity
                        style={styles.toggleButton}
                        onPress={() => setShowPassword(!showPassword)}
                        accessibilityLabel="Toggle password visibility"
                    >
                        <MaterialIcons name={showPassword ? 'visibility-off' : 'visibility'} size={24} color="grey" />
                    </TouchableOpacity>
                </View>
                <View style={styles.passwordInputContainer}>
                    <TextInput
                        style={styles.passwordInput}
                        placeholder="Confirm Password"
                        onChangeText={setConfirmPassword}
                        value={confirmPassword}
                        secureTextEntry={!showConfirmPassword}
                    />
                    <TouchableOpacity
                        style={styles.toggleButton}
                        onPress={() => setShowConfirmPassword(!showConfirmPassword)}
                        accessibilityLabel="Toggle confirm password visibility"
                    >
                        <MaterialIcons name={showConfirmPassword ? 'visibility-off' : 'visibility'} size={24} color="grey" />
                    </TouchableOpacity>
                </View>

                <TouchableOpacity style={styles.button} onPress={handleRegister} disabled={loading || registrationSuccess}>
                    {loading ? (
                        <ActivityIndicator color="#ffffff" />
                    ) : (
                        <View style={styles.buttonContent}>
                            <MaterialIcons name="person-add" size={24} color="white" />
                            <Text style={styles.buttonText}>Register</Text>
                        </View>
                    )}
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={styles.loginText}>Already have an account? Login here</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
};


const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
    content: {
        width: '100%',
        maxWidth: 400,
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 24,
        marginBottom: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    input: {
        width: '100%',
        height: 50,
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        marginBottom: 20,
        paddingHorizontal: 15,
        fontSize: 16,
        backgroundColor: '#ffffff',
    },
    passwordInputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 50,
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        marginBottom: 20,
        paddingHorizontal: 15,
        backgroundColor: '#ffffff',
    },
    passwordInput: {
        flex: 1,
        fontSize: 16,
    },
    toggleButton: {
        padding: 10,
    },
    button: {
        width: '100%',
        height: 50,
        backgroundColor: '#007bff',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginBottom: 20,
    },
    buttonContent: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    buttonText: {
        color: '#ffffff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 5,
    },
    loginText: {
        color: '#007bff',
        fontSize: 16,
        textAlign: 'center',
    },
    errorText: {
        color: 'red',
        fontSize: 16,
        marginBottom: 10,
        textAlign: 'center',
    },
    successText: {
        color: 'green',
        fontSize: 16,
        marginBottom: 10,
        textAlign: 'center',
    },
});

export default RegisterScreen;