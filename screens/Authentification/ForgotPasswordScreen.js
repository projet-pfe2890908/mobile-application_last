import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  RefreshControl,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { sendPasswordResetEmail } from "firebase/auth";
import { auth } from "../../firebaseConfig";

const ForgotPasswordScreen = () => {
  const navigation = useNavigation();
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [passwordReset, setPasswordReset] = useState(null);
  const [refreshing, setRefreshing] = useState(false);

  const handleResetPassword = async () => {
    try {
      console.log("Resetting password...");
      setLoading(true);

      // Send password reset email directly
      await sendPasswordResetEmail(auth, email);
      console.log("Password reset email sent successfully.");
      setPasswordReset(
        "Password reset email sent successfully. Please check your email to reset your password."
      );
      setError(null); // Clear any previous error
    } catch (error) {
      console.error("Error resetting password:", error); // Log the error
      setError("Error resetting password. Please try again."); // Set error message
    } finally {
      setLoading(false);
    }
  };

  const onRefresh = () => {
    setEmail("");
    setLoading(false);
    setError(null);
    setPasswordReset(null);
  };

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      <View style={styles.content}>
        <Text style={styles.title}>Forgot Password</Text>
        {error && <Text style={styles.errorText}>{error}</Text>}
        {passwordReset && (
          <Text style={styles.successText}>{passwordReset}</Text>
        )}
        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={setEmail}
          value={email}
          keyboardType="email-address"
          autoCapitalize="none"
          testID="emailInput"
        />
        <TouchableOpacity
          style={styles.button}
          onPress={handleResetPassword}
          disabled={loading}
          testID="resetPasswordButton"
        >
          <Text style={styles.buttonText}>Reset Password</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Text style={styles.backText} testID="backButton">
            Back to Login
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  contentContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    width: "80%",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
    fontWeight: "bold",
    color: "#333333",
  },
  input: {
    width: "100%",
    height: 50,
    borderColor: "#cccccc",
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 15,
    fontSize: 16,
    backgroundColor: "#ffffff",
  },
  button: {
    width: "100%",
    height: 50,
    backgroundColor: "#007bff",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    marginBottom: 20,
  },
  buttonText: {
    color: "#ffffff",
    fontSize: 18,
    fontWeight: "bold",
  },
  backText: {
    color: "#007bff",
    fontSize: 16,
    textDecorationLine: "underline",
  },
  errorText: {
    color: "red",
    fontSize: 16,
    marginBottom: 10,
  },
  successText: {
    color: "green",
    fontSize: 16,
    marginBottom: 10,
  },
});

export default ForgotPasswordScreen;
