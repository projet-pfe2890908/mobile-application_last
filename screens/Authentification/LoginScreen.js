import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, ActivityIndicator, StyleSheet, ScrollView, RefreshControl } from 'react-native';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth, firestore } from '../../firebaseConfig';
import { getDoc, doc } from 'firebase/firestore';
import { MaterialIcons } from '@expo/vector-icons'; // Import MaterialIcons from Expo

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [loginSuccess, setLoginSuccess] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    const clearFormAndState = () => {
      setPassword('');
      setLoading(false);
      setError(null);
      setLoginSuccess(false);
      setShowPassword(false);
    };
  
    const onScreenFocus = navigation.addListener('focus', clearFormAndState);

    return () => {
      onScreenFocus();
    };
  }, [navigation]);

  const handleLogin = async () => {
    try {
      setLoading(true);
      if (!email.trim() || !password.trim()) {
        throw new Error('Please enter your email and password.');
      }
      const userCredential = await signInWithEmailAndPassword(auth, email, password);

      const user = userCredential.user; // Extracting the user object from the userCredential
      if (user && !user.emailVerified) {
        navigation.navigate('VerifyEmail');
        return;
      }
      
      setLoginSuccess(true);
      setError(null);

      const userDocRef = doc(firestore, 'users', userCredential.user.uid);
      const userDocSnap = await getDoc(userDocRef);

      navigation.navigate(userDocSnap.exists() && userDocSnap.data().setupCompleted ? 'ProfileSelection' : 'Setup');
    } catch (error) {
      console.error('Login failed:', error);
      setError('Login failed. Please check your email and password.');
    } finally {
      setLoading(false);
    }
  };

  const onRefresh = () => {
    setRefreshing(true);
    setPassword('');
    setLoading(false);
    setError(null);
    setLoginSuccess(false);
    setRefreshing(false);
  };

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
      refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
    >
      <View style={styles.content}>
        <Text style={styles.title}>Login</Text>
        {error && <Text style={styles.errorText}>{error}</Text>}
        {loginSuccess && <Text style={styles.successText}>Login successful!</Text>}
        <View style={styles.inputContainer}>
          <MaterialIcons name="email" size={24} color="#777" style={styles.icon} />
          <TextInput
            style={styles.input}
            placeholder="Email"
            onChangeText={setEmail}
            value={email}
            keyboardType="email-address"
            autoCapitalize="none"
            testID="emailInput"
            autoFocus // Auto-focus on email input field
          />
        </View>
        <View style={styles.inputContainer}>
          <MaterialIcons name="lock" size={24} color="#777" style={styles.icon} />
          <TextInput
            style={styles.input}
            placeholder="Password"
            onChangeText={setPassword}
            value={password}
            secureTextEntry={!showPassword}
            testID="passwordInput"
          />
          <TouchableOpacity
            style={styles.toggleButton}
            onPress={() => setShowPassword(!showPassword)}
            accessibilityLabel={showPassword ? "Hide password" : "Show password"} // Accessibility label for password visibility toggle
          >
            <MaterialIcons name={showPassword ? 'visibility-off' : 'visibility'} size={24} color="#777" />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.button} onPress={handleLogin} disabled={loading || loginSuccess} testID="loginButton">
          {loading ? (
            <ActivityIndicator color="#ffffff" />
          ) : (
            <Text style={styles.buttonText}>Login</Text>
          )}
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')}>
          <Text style={styles.forgotPasswordText}>Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
          <Text style={styles.registerText}>Don't have an account? Register here</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: '80%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: 50,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 15,
    backgroundColor: '#ffffff',
  },
  input: {
    flex: 1,
    fontSize: 16,
  },
  icon: {
    marginRight: 10,
  },
  toggleButton: {
    padding: 10,
  },
  button: {
    width: '100%',
    height: 50,
    backgroundColor: '#007bff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 20,
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  registerText: {
    marginTop: 10,
    color: '#007bff',
    fontSize: 16,
  },
  errorText: {
    color: 'red',
    fontSize: 16,
    marginBottom: 10,
  },
  successText: {
    color: 'green',
    fontSize: 16,
    marginBottom: 10,
  },
  forgotPasswordText: {
    color: '#007bff',
    textDecorationLine: 'underline',
  },
});

export default LoginScreen;
