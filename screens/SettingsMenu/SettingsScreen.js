import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import { signOut } from 'firebase/auth';
import { auth } from '../../firebaseConfig';
import { Ionicons } from '@expo/vector-icons';

const SettingsScreen = ({ navigation }) => {

    // Function to handle navigation to the car linking screen
    const handleLinkToCar = () => {
        navigation.navigate('CarLinking');
    };

    // Function to handle navigation to the manage profiles screen
    const handleManageProfiles = () => {
        navigation.navigate('ManageProfiles');
    };

    // Function to handle navigation to the delete link screen
    const handleDeleteLink = () => {
        navigation.navigate('DeleteLink');
    };

    // Function to handle logout
    const handleLogout = async () => {
        Alert.alert(
            'Logout',
            'Are you sure you want to logout?',
            [
                { text: 'Cancel', style: 'cancel' },
                {
                    text: 'Logout',
                    onPress: async () => {
                        try {
                            await signOut(auth);
                            navigation.navigate('Login'); // Navigate to the login screen
                            console.log('User signed out successfully');
                        } catch (error) {
                            console.error('Failed to log out:', error.message);
                            // Handle logout failure, if needed
                        }
                    },
                },
            ],
            { cancelable: false }
        );
    };

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Settings</Text>
            <TouchableOpacity style={styles.optionButton} onPress={handleManageProfiles}>
                <Ionicons name="person-outline" size={24} color="#ffffff" />
                <Text style={styles.optionButtonText}>Manage Profiles</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.optionButton} onPress={handleLinkToCar}>
                <Ionicons name="car-outline" size={24} color="#ffffff" />
                <Text style={styles.optionButtonText}>Link to Another Car</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.optionButton} onPress={handleDeleteLink}>
                <Ionicons name="trash-outline" size={24} color="#ffffff" />
                <Text style={styles.optionButtonText}>Delete a Link</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.logoutButton} onPress={handleLogout}>
                <Ionicons name="log-out-outline" size={24} color="#ffffff" />
                <Text style={styles.logoutButtonText}>Logout</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        paddingHorizontal: 20,
        paddingTop: 60,
        paddingBottom: 40,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
    },
    optionButton: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 50,
        backgroundColor: '#007bff',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        marginBottom: 10,
    },
    optionButtonText: {
        color: '#ffffff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 10,
    },
    logoutButton: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 50,
        backgroundColor: '#dc3545',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    logoutButtonText: {
        color: '#ffffff',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 10,
    },
});

export default SettingsScreen;
