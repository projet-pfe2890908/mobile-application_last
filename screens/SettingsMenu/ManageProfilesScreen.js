import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Modal, TextInput, Alert } from 'react-native';
import { doc, updateDoc, collection, deleteDoc, getDocs } from 'firebase/firestore';
import { auth, firestore } from '../../firebaseConfig';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

const ManageProfilesScreen = () => {
    const [profiles, setProfiles] = useState([]);
    const [selectedProfile, setSelectedProfile] = useState(null);
    const [editedFirstName, setEditedFirstName] = useState('');
    const [editedLastName, setEditedLastName] = useState('');
    const [isEditModalVisible, setIsEditModalVisible] = useState(false);
    const navigation = useNavigation();

    useEffect(() => {
        fetchProfiles();
    }, []);

    const fetchProfiles = async () => {
        try {
            const userDocRef = doc(firestore, 'users', auth.currentUser.uid);
            const profilesCollectionRef = collection(userDocRef, 'profiles');
            const profilesQuerySnapshot = await getDocs(profilesCollectionRef);

            const fetchedProfiles = [];
            profilesQuerySnapshot.forEach((doc) => {
                fetchedProfiles.push({ id: doc.id, ...doc.data() });
            });

            setProfiles(fetchedProfiles);
        } catch (error) {
            console.error('Error fetching profiles:', error);
        }
    };

    const handleProfileEdit = (profile) => {
        setSelectedProfile(profile);
        setEditedFirstName(profile.firstName);
        setEditedLastName(profile.lastName);
        setIsEditModalVisible(true);
    };

    const handleEditConfirm = async () => {
        try {
            // Update profile in Firestore
            const profileDocRef = doc(firestore, 'users', auth.currentUser.uid, 'profiles', selectedProfile.id);
            await updateDoc(profileDocRef, {
                firstName: editedFirstName,
                lastName: editedLastName
            });
            // Update local state
            setProfiles(profiles.map(p => {
                if (p.id === selectedProfile.id) {
                    return {
                        ...p,
                        firstName: editedFirstName,
                        lastName: editedLastName
                    };
                }
                return p;
            }));
            // Close modal
            setIsEditModalVisible(false);
            setSelectedProfile(null);
        } catch (error) {
            console.error('Error editing profile:', error);
            // Handle error
        }
    };
    
    const handleProfileDelete = async (profile) => {
        Alert.alert(
            'Delete Profile',
            `Are you sure you want to delete the profile ${profile.firstName} ${profile.lastName}?`,
            [
                {
                    text: 'Cancel',
                    style: 'cancel'
                },
                {
                    text: 'Delete',
                    onPress: async () => {
                        try {
                            // Delete profile from Firestore
                            const profileDocRef = doc(firestore, 'users', auth.currentUser.uid, 'profiles', profile.id);
                            await deleteDoc(profileDocRef);
                            // Update local state
                            setProfiles(profiles.filter(p => p.id !== profile.id));
                            console.log('Profile deleted successfully');
                        } catch (error) {
                            console.error('Error deleting profile:', error);
                            // Handle error
                        }
                    }
                }
            ]
        );
    };

    const renderProfileItem = ({ item }) => (
        <View style={styles.profileItem}>
            <Ionicons name="person-circle-outline" size={30} color="#007bff" style={styles.profileIcon} />
            <TouchableOpacity style={styles.profileItemContent}>
                <Text style={styles.profileItemText}>{item.firstName} {item.lastName}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleProfileEdit(item)} style={styles.profileAction}>
                <Ionicons name="create-outline" size={24} color="#007bff" />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => handleProfileDelete(item)} style={styles.profileAction}>
                <Ionicons name="trash-outline" size={24} color="#dc3545" />
            </TouchableOpacity>
        </View>
    );

    const handleCreateProfile = () => {
        navigation.navigate('ProfileCreation');
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.title}>Manage Profiles</Text>
                <TouchableOpacity onPress={handleCreateProfile}>
                    <Ionicons name="person-add-outline" size={30} color="#007bff" />
                </TouchableOpacity>
            </View>
            <FlatList
                data={profiles}
                renderItem={renderProfileItem}
                keyExtractor={(item) => item.id}
            />
            {/* Edit Profile Modal */}
            <Modal
                visible={isEditModalVisible}
                animationType="slide"
                transparent={true}
                onRequestClose={() => setIsEditModalVisible(false)}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.modalContent}>
                        <Text style={styles.modalTitle}>Edit Profile</Text>
                        <TextInput
                            style={styles.input}
                            placeholder="First Name"
                            onChangeText={setEditedFirstName}
                            value={editedFirstName}
                        />
                        <TextInput
                            style={styles.input}
                            placeholder="Last Name"
                            onChangeText={setEditedLastName}
                            value={editedLastName}
                        />
                        <TouchableOpacity style={styles.modalButton} onPress={handleEditConfirm}>
                            <Text style={styles.modalButtonText}>Confirm</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        paddingHorizontal: 20,
        paddingTop: 60,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    profileItem: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
    },
    profileIcon: {
        marginRight: 10,
    },
    profileItemContent: {
        flex: 1,
    },
    profileItemText: {
        fontSize: 18,
    },
    profileAction: {
        paddingHorizontal: 10,
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: '#ffffff',
        padding: 20,
        borderRadius: 10,
        elevation: 5,
        width: '80%',
    },
    modalTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    input: {
        height: 40,
        borderColor: '#cccccc',
        borderWidth: 1,
        borderRadius: 5,
        marginBottom: 10,
        paddingHorizontal: 10,
    },
    modalButton: {
        backgroundColor: '#007bff',
        paddingVertical: 10,
        borderRadius: 5,
        alignItems: 'center',
    },
    modalButtonText: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: 'bold',
    },
});

export default ManageProfilesScreen;
