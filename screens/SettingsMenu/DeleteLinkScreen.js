import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet, Alert, Image } from 'react-native';
import { doc, getDoc, updateDoc } from 'firebase/firestore';
import { auth, firestore } from '../../firebaseConfig';
import { Ionicons } from '@expo/vector-icons'; // Import Ionicons for icons

const DeleteLinkScreen = ({ navigation }) => {
    const [linkedCars, setLinkedCars] = useState([]);
    const [carData, setCarData] = useState({});

    useEffect(() => {
        fetchLinkedCars();
    }, []);

    const fetchLinkedCars = async () => {
        try {
            const userDocRef = doc(firestore, 'users', auth.currentUser.uid);
            const userDocSnap = await getDoc(userDocRef);
            const userData = userDocSnap.data();
            if (userData && userData.linked_cars) {
                setLinkedCars(userData.linked_cars);
                fetchCarData(userData.linked_cars);
            }
        } catch (error) {
            console.error('Error fetching linked cars:', error);
        }
    };

    const fetchCarData = async (carIds) => {
        try {
            const carData = {};
            for (const carId of carIds) {
                const carDocRef = doc(firestore, 'cars', carId);
                const carDocSnap = await getDoc(carDocRef);
                if (carDocSnap.exists()) {
                    const carInfo = carDocSnap.data();
                    const imageURL = carInfo.image_url; // Assuming 'image_url' is the field containing the image URL
                    carData[carId] = { ...carInfo, imageURL };
                }
            }
            setCarData(carData);
        } catch (error) {
            console.error('Error fetching car data:', error);
        }
    };

    const handleDeleteLink = (carId) => {
        Alert.alert(
            'Confirm Deletion',
            'Are you sure you want to delete this link?',
            [
                { text: 'Cancel', style: 'cancel' },
                { text: 'Delete', onPress: () => deleteLink(carId) },
            ]
        );
    };

    const deleteLink = async (carId) => {
        try {
            // Remove the car from the user's linked cars
            const updatedLinkedCars = linkedCars.filter(id => id !== carId);
            const userDocRef = doc(firestore, 'users', auth.currentUser.uid);
            await updateDoc(userDocRef, { linked_cars: updatedLinkedCars });
    
            // Remove owner_id from the deleted car
            const carDocRef = doc(firestore, 'cars', carId);
            await updateDoc(carDocRef, { owner_id: null });
    
            // Navigate back to the settings screen
            navigation.goBack();
        } catch (error) {
            console.error('Error deleting link:', error);
        }
    };
    
    const renderItem = ({ item }) => (
        <TouchableOpacity style={styles.item} onPress={() => handleDeleteLink(item)}>
            <Image source={{ uri: item.imageURL }} style={styles.image} />
            <Text style={styles.itemText}>
                {`${item.make} ${item.model}`}
            </Text>
            <Ionicons name="trash-outline" size={24} color="#dc3545" style={styles.deleteIcon} />
        </TouchableOpacity>
    );    

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Delete a Link</Text>
            <FlatList
                data={Object.values(carData)}
                renderItem={renderItem}
                keyExtractor={item => item.carId}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingTop: 40,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
    },
    item: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
    },
    itemText: {
        fontSize: 18,
        flex: 1, // Adjusted to make text take remaining space
    },
    image: {
        width: 110,
        height: 50,
        marginRight: 10,
    },
    deleteIcon: {
        marginLeft: 'auto', // Align icon to the right
    },
});

export default DeleteLinkScreen;
