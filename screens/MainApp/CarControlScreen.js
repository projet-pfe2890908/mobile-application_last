import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Alert,
  Modal,
  FlatList,
  AppState,
  ScrollView,
  RefreshControl,
} from "react-native";
import { doc, updateDoc, getDoc } from "firebase/firestore";
import { firestore, auth } from "../../firebaseConfig";
import { Ionicons, MaterialIcons } from "@expo/vector-icons"; // Import additional icon library for more icons

const CarControlScreen = ({ route, navigation }) => {
  const { carId, profileId } = route.params;
  const [carData, setCarData] = useState(null);
  const [presets, setPresets] = useState([]);
  const [selectedPreset, setSelectedPreset] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [appState, setAppState] = useState(AppState.currentState);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      fetchCarData();
      fetchPresets();
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener("beforeRemove", (e) => {
      // Prevent default behavior of leaving the screen
      e.preventDefault();

      // Clear selected_profile_id in car document
      clearSelectedProfileId();

      // Continue with the navigation
      navigation.dispatch(e.data.action);
    });

    const handleAppStateChange = (nextAppState) => {
      if (appState.match(/inactive|background/) && nextAppState === "active") {
        // App has come to the foreground
        // Navigate back to the select car screen
        navigation.navigate("CarSelection");
      } else if (
        appState === "active" &&
        nextAppState.match(/inactive|background/)
      ) {
        // App has gone to the background
        // Clear selected_profile_id in car document
        clearSelectedProfileId();
      }
      setAppState(nextAppState);
    };

    // Subscribe to app state changes
    const appStateSubscription = AppState.addEventListener(
      "change",
      handleAppStateChange
    );

    // Clean up the subscription
    return () => {
      appStateSubscription.remove();
      unsubscribe();
    };
  }, [navigation, appState]);

  const fetchCarData = async () => {
    try {
      const carDocRef = doc(firestore, "cars", carId);
      const carDocSnap = await getDoc(carDocRef);
      if (carDocSnap.exists()) {
        setCarData(carDocSnap.data());
      }
    } catch (error) {
      console.error("Error fetching car data:", error);
    }
  };

  const fetchPresets = async () => {
    try {
      const profileDocRef = doc(
        firestore,
        "users",
        auth.currentUser.uid,
        "profiles",
        profileId
      );
      const profileDocSnap = await getDoc(profileDocRef);
      if (profileDocSnap.exists()) {
        const profileData = profileDocSnap.data();
        if (profileData.presets) {
          setPresets(profileData.presets);
        }
      }
    } catch (error) {
      console.error("Error fetching presets:", error);
    }
  };

  const clearSelectedProfileId = async () => {
    try {
      const carDocRef = doc(firestore, "cars", carId);
      await updateDoc(carDocRef, { selected_profile_id: null });
      console.log("Selected profile ID cleared successfully");
    } catch (error) {
      console.error("Error clearing selected profile ID:", error);
    }
  };

  const handlePresetSelection = async (selectedPreset) => {
    setSelectedPreset(selectedPreset);
    setModalVisible(false);
    console.log("Selected preset:", selectedPreset);
    // Save the ID of the selected preset as the default preset in Firestore
    try {
      const profileDocRef = doc(
        firestore,
        "users",
        auth.currentUser.uid,
        "profiles",
        profileId
      );
      if (selectedPreset) {
        const presetId = selectedPreset.id || selectedPreset.name; // Use name as a fallback if id is not available
        await updateDoc(profileDocRef, { default_preset_id: presetId });
        console.log("Default preset updated successfully:", presetId);
      } else {
        console.warn("Invalid preset:", selectedPreset);
      }
    } catch (error) {
      console.error("Error saving default preset:", error);
    }
  };

  const handleLockUnlock = async () => {
    try {
      const updatedControls = { ...carData.controls };
      updatedControls.locked = !carData.controls.locked;
      const carDocRef = doc(firestore, "cars", carId);
      await updateDoc(carDocRef, { controls: updatedControls });
      setCarData({ ...carData, controls: updatedControls });

      // If the car is unlocked and a default preset is selected, ask for confirmation
      if (!updatedControls.locked && selectedPreset) {
        Alert.alert(
          "Apply Default Preset",
          "Do you want to apply your default preset?",
          [
            {
              text: "Cancel",
              style: "cancel",
            },
            {
              text: "Apply",
              onPress: () => applyDefaultPreset(),
            },
          ],
          { cancelable: false }
        );
      }

      console.log("Lock/Unlock successful");
    } catch (error) {
      console.error("Error updating lock status:", error);
      Alert.alert("Error", "Failed to update lock status. Please try again.");
    }
  };

  const applyDefaultPreset = async () => {
    try {
      // Retrieve the default preset ID from the selected profile
      const profileDocRef = doc(
        firestore,
        "users",
        auth.currentUser.uid,
        "profiles",
        profileId
      );
      const profileDocSnap = await getDoc(profileDocRef);
      if (profileDocSnap.exists()) {
        const profileData = profileDocSnap.data();
        const defaultPresetId = profileData.default_preset_id;

        // Retrieve the default preset from the presets array
        const preset = profileData.presets.find(
          (preset) => preset.name === defaultPresetId
        );

        // Apply the default preset to the car controls
        if (preset) {
          const carDocRef = doc(firestore, "cars", carId);
          await updateDoc(carDocRef, { settings: preset.settings });
          console.log("Default preset applied successfully");
        } else {
          console.warn("Default preset not found:", defaultPresetId);
        }
      } else {
        console.error("Profile document not found");
      }
    } catch (error) {
      console.error("Error applying default preset:", error);
    }
  };

  const handleRemovePreset = async (presetToRemove) => {
    try {
      // Remove the preset from the database
      const profileDocRef = doc(
        firestore,
        "users",
        auth.currentUser.uid,
        "profiles",
        profileId
      );
      const profileDocSnap = await getDoc(profileDocRef);

      if (profileDocSnap.exists()) {
        const profileData = profileDocSnap.data();
        const updatedPresets = profileData.presets.filter(
          (preset) => preset.name !== presetToRemove.name
        );
        await updateDoc(profileDocRef, { presets: updatedPresets });

        // Update the local state to reflect the removal of the preset
        setPresets(updatedPresets);
        console.log("Preset removed successfully:", presetToRemove.name);
      } else {
        console.error("Profile document not found");
      }
    } catch (error) {
      console.error("Error removing preset:", error);
    }
  };

  const navigateToPresetCreation = () => {
    navigation.navigate("PresetCreation", { carData });
  };

  const navigateToSettings = (settingType) => {
    navigation.navigate("CarSettings", { carData, settingType });
  };

  const onRefresh = () => {
    setRefreshing(true);
    fetchCarData();
    fetchPresets();
    setRefreshing(false);
  };

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      <View style={styles.carInfo}>
        {carData && (
          <Image source={{ uri: carData.image_url }} style={styles.carImage} />
        )}
        <Text style={styles.carMakeModel}>
          {carData ? `${carData.make} ${carData.model}` : "Car Model"}
        </Text>
      </View>

      <TouchableOpacity
        style={styles.presetDropdown}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.dropdownText}>
          {selectedPreset ? selectedPreset.name : "Select Default Preset"}
        </Text>
        <Ionicons name="caret-down-outline" size={24} color="#333" />
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <TouchableOpacity
              onPress={() => setModalVisible(false)}
              style={styles.closeButton}
            >
              <Ionicons name="close-circle-outline" size={24} color="#333" />
            </TouchableOpacity>
            <Text style={styles.modalTitle}>Select Preset</Text>
            <FlatList
              data={presets}
              keyExtractor={(item) => item.name}
              renderItem={({ item }) => (
                <View style={styles.presetItemContainer}>
                  <TouchableOpacity
                    style={styles.presetItem}
                    onPress={() => handlePresetSelection(item)}
                  >
                    <Text style={styles.presetItemText}>{item.name}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.deleteIconContainer}
                    onPress={() => handleRemovePreset(item)}
                  >
                    <Ionicons name="trash-outline" size={24} color="#ff0000" />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>
        </View>
      </Modal>
      <TouchableOpacity
        style={[
          styles.controlButton,
          { backgroundColor: carData?.controls.locked ? "#dc3545" : "#28a745" },
        ]}
        onPress={handleLockUnlock}
      >
        <Ionicons
          name={
            carData?.controls.locked
              ? "lock-closed-outline"
              : "lock-open-outline"
          }
          size={24}
          color="#fff"
        />
        <Text style={styles.controlButtonText}>
          {carData?.controls.locked ? "Unlock" : "Lock"}
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.createPresetButton}
        onPress={navigateToPresetCreation}
      >
        <Ionicons name="add-circle-outline" size={24} color="#007bff" />
        <Text style={styles.createPresetButtonText}>Create Preset</Text>
      </TouchableOpacity>

      <View style={styles.settingsButtonsContainer}>
        <TouchableOpacity
          style={styles.settingButton}
          onPress={() => navigateToSettings("seat")}
        >
          <MaterialIcons
            name="airline-seat-recline-extra"
            size={40}
            color="#4CAF50"
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.settingButton}
          onPress={() => navigateToSettings("light")}
        >
          <Ionicons name="sunny-outline" size={40} color="#ffc107" />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.settingButton}
          onPress={() => navigateToSettings("climate")}
        >
          <MaterialIcons name="ac-unit" size={40} color="#007bff" />
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
  },
  contentContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  carInfo: {
    alignItems: "center",
    marginBottom: 20,
  },
  carImage: {
    width: 350,
    height: 165,
    resizeMode: "cover",
    marginBottom: 10,
  },
  carMakeModel: {
    fontSize: 18,
    fontWeight: "bold",
    textAlign: "center",
  },
  presetDropdown: {
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#ccc",
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderRadius: 8,
    marginBottom: 20,
    width: "80%",
  },
  dropdownText: {
    flex: 1,
    fontSize: 16,
    color: "#333",
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalContent: {
    backgroundColor: "#fff",
    padding: 20,
    borderRadius: 10,
    width: "80%",
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
    textAlign: "center",
  },
  presetItemContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
  },
  presetItem: {
    flex: 1,
  },
  presetItemText: {
    fontSize: 16,
  },
  deleteIconContainer: {
    marginLeft: 10,
  },
  closeButton: {
    position: "absolute",
    top: 10,
    right: 10,
  },

  controlButton: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#28a745",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    marginBottom: 10,
  },
  controlButtonText: {
    marginLeft: 10,
    fontSize: 18,
    fontWeight: "bold",
    color: "#fff",
  },
  createPresetButton: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    borderColor: "#007bff",
    borderWidth: 1,
  },
  createPresetButtonText: {
    marginLeft: 10,
    fontSize: 18,
    fontWeight: "bold",
    color: "#007bff",
  },
  settingsButtonsContainer: {
    flexDirection: "row",
    justifyContent: "space-around", // Evenly space buttons
    marginTop: 20,
    width: "80%",
  },
  settingButton: {
    alignItems: "center", // Center icon within the button
  },
});

export default CarControlScreen;
