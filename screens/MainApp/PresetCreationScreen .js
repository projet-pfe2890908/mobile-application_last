import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from "react-native";
import {
  doc,
  updateDoc,
  getDoc,
  getDocs,
  collection,
} from "firebase/firestore";
import { auth, firestore } from "../../firebaseConfig"; // Import auth from firebaseConfig
import { Ionicons } from "@expo/vector-icons"; // Import Ionicons from @expo/vector-icons

const PresetCreationScreen = ({ route, navigation }) => {
  const { carData } = route.params;
  const [presetName, setPresetName] = useState("");
  useEffect(() => {
    // Show the pop-up alert when the screen is focused
    const unsubscribe = navigation.addListener("focus", () => {
      Alert.alert(
        "Important Reminder",
        "To create a preset, please make sure you are inside the car with the key and adjust the settings as you prefer.",
        [
          {
            text: "OK",
            onPress: () => console.log("Alert closed"),
          },
        ]
      );
    });

    return unsubscribe; // Clean up the listener when the component unmounts
  }, [navigation]); // Dependency on navigation ensures the alert is shown on every focus

  const handleSavePreset = async () => {
    try {
      console.log("Car Data:", carData); // Add this line to check carData structure
      // Check if the carData object exists
      if (!carData) {
        console.error("Car data is undefined or null.");
        Alert.alert(
          "Error",
          "Failed to save preset. Car data is undefined or null."
        );
        return;
      }
      // Check if the car has key inside
      if (!carData.key_status) {
        console.error("Key not inside the car.");
        Alert.alert(
          "Error",
          "Failed to save preset. Key must be inside the car."
        );
        return;
      }

      console.log("Car Settings:", carData.settings); // Add this line to check carData.settings structure

      // Check if carData.settings object exists
      if (!carData.settings) {
        console.error("Car settings are undefined or null.");
        Alert.alert(
          "Error",
          "Failed to save preset. Car settings are undefined or null."
        );
        return;
      }

      // Inside handleSavePreset function

      const preset = {
        name: presetName,
        settings: {
          ambient_light_control: {
            ambient_light_status:
              carData.settings.ambient_light_control.ambient_light_status,
            ambient_light_color_red:
              carData.settings.ambient_light_control.ambient_light_color_red,
            ambient_light_color_green:
              carData.settings.ambient_light_control.ambient_light_color_green,
            ambient_light_color_blue:
              carData.settings.ambient_light_control.ambient_light_color_blue,
            ambient_light_brightness:
              carData.settings.ambient_light_control.ambient_light_brightness,
          },
          seat_controls: {
            forward_backward_adjustment:
              carData.settings.seat_controls.forward_backward_adjustment,
            recline_angle_adjustment:
              carData.settings.seat_controls.recline_angle_adjustment,
            height_adjustment: carData.settings.seat_controls.height_adjustment,
            seat_heating: carData.settings.seat_controls.seat_heating,
            seat_ventilation: carData.settings.seat_controls.seat_ventilation,
          },
          climate_control: {
            temperature_setting:
              carData.settings.climate_control.temperature_setting,
            auto_program_active:
              carData.settings.climate_control.auto_program_active,
            maximum_cooling_active:
              carData.settings.climate_control.maximum_cooling_active,
            cooling_function_active:
              carData.settings.climate_control.cooling_function_active,
            rear_window_defroster_active:
              carData.settings.climate_control.rear_window_defroster_active,
            defrost_windows_active:
              carData.settings.climate_control.defrost_windows_active,
            system_power_status:
              carData.settings.climate_control.system_power_status,
            fan_speed: carData.settings.climate_control.fan_speed,
          },
        },
      };

      // Retrieve the user document
      const userDocRef = doc(firestore, "users", auth.currentUser.uid);
      const userDocSnap = await getDoc(userDocRef);

      if (userDocSnap.exists()) {
        // User document exists, retrieve the active profile
        const activeProfileSnapshot = await getDocs(
          collection(userDocRef, "profiles")
        );
        const activeProfileDoc = activeProfileSnapshot.docs.find(
          (doc) => doc.data().is_active === true
        );

        if (activeProfileDoc) {
          // Update the presets array of the active profile
          const activeProfileRef = doc(
            userDocRef,
            "profiles",
            activeProfileDoc.id
          );
          const activeProfileData = activeProfileDoc.data();
          const updatedPresets = [...activeProfileData.presets, preset]; // Directly add the preset to the existing array
          await updateDoc(activeProfileRef, { presets: updatedPresets });
          console.log("Preset saved successfully");
          navigation.goBack();
        } else {
          // No active profile found, display an error
          console.error("Active profile not found");
          Alert.alert(
            "Error",
            "Failed to save preset. Active profile not found."
          );
        }
      } else {
        // User document not found, display an error
        console.error("User document not found");
        Alert.alert("Error", "Failed to save preset. User document not found.");
      }
    } catch (error) {
      console.error("Error saving preset:", error);
      Alert.alert("Error", "Failed to save preset. Please try again.");
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Create Preset</Text>
      <View style={styles.inputContainer}>
        <Ionicons
          name="document-text-outline"
          size={24}
          color="#007bff"
          style={styles.icon}
        />
        <TextInput
          style={styles.input}
          placeholder="Preset Name"
          value={presetName}
          onChangeText={setPresetName}
        />
      </View>
      <TouchableOpacity style={styles.saveButton} onPress={handleSavePreset}>
        <Ionicons
          name="save-outline"
          size={24}
          color="#ffffff"
          style={styles.icon}
        />
        <Text style={styles.saveButtonText}>Save Preset</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    paddingHorizontal: 20,
    paddingTop: 60,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  inputContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20,
  },
  icon: {
    marginRight: 10,
  },
  input: {
    flex: 1,
    height: 40,
    borderColor: "#cccccc",
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
  },
  saveButton: {
    flexDirection: "row",
    backgroundColor: "#007bff",
    paddingVertical: 10,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
  saveButtonText: {
    color: "#ffffff",
    fontSize: 18,
    fontWeight: "bold",
    marginLeft: 5,
  },
});

export default PresetCreationScreen;
