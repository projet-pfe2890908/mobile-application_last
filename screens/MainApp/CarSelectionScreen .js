import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  RefreshControl,
  Image,
} from "react-native";
import {
  doc,
  collection,
  getDocs,
  query,
  where,
  updateDoc,
} from "firebase/firestore";
import { auth, firestore } from "../../firebaseConfig";
import { useFocusEffect } from "@react-navigation/native";
import { AppState } from "react-native";

const CarSelectionScreen = ({ navigation, route }) => {
  const { profileId } = route.params; // Extract profileId from route params
  const [cars, setCars] = useState([]);
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [appState, setAppState] = useState(AppState.currentState);
  const [error, setError] = useState(null); // Define error state variable

  useEffect(() => {
    fetchCars();

    const handleAppStateChange = (nextAppState) => {
      if (appState.match(/inactive|background/) && nextAppState === "active") {
        // App has come to the foreground
        if (profileId) {
          const profileDocRef = doc(
            firestore,
            "users",
            auth.currentUser.uid,
            "profiles",
            profileId
          );
          updateDoc(profileDocRef, { is_active: true });
        }
      } else if (
        appState === "active" &&
        nextAppState.match(/inactive|background/)
      ) {
        // App has gone to the background
        if (profileId) {
          const profileDocRef = doc(
            firestore,
            "users",
            auth.currentUser.uid,
            "profiles",
            profileId
          );
          updateDoc(profileDocRef, { is_active: false });
        }
      }

      setAppState(nextAppState);
    };

    const unsubscribe = navigation.addListener("beforeRemove", () => {
      // Update is_active field in the selected profile document to false
      if (profileId) {
        const profileDocRef = doc(
          firestore,
          "users",
          auth.currentUser.uid,
          "profiles",
          profileId
        );
        updateDoc(profileDocRef, { is_active: false });
      }
    });

    // Subscribe to app state changes
    const appStateSubscription = AppState.addEventListener(
      "change",
      handleAppStateChange
    );

    // Clean up the subscription
    return () => {
      appStateSubscription.remove();
      unsubscribe();
    };
  }, [navigation, appState, profileId]);

  const fetchCars = async () => {
    try {
      setLoading(true);
      const carsCollectionRef = collection(firestore, "cars");
      const userCarsQuery = query(
        carsCollectionRef,
        where("owner_id", "==", auth.currentUser.uid)
      );
      const carsQuerySnapshot = await getDocs(userCarsQuery);
      const carsData = carsQuerySnapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));
      setCars(carsData);
    } catch (error) {
      console.error("Error fetching cars:", error);
    } finally {
      setLoading(false);
    }
  };

  // Fetch cars when the screen comes into focus
  useFocusEffect(
    React.useCallback(() => {
      fetchCars();
    }, [])
  );

  const handleCarSelection = async (car) => {
    try {
      if (!profileId) {
        console.error("No profileId provided.");
        return;
      }

      // Check if the car already has a selected_profile_id
      if (car.selected_profile_id) {
        setError("This car is currently in use.");
        return;
      }

      // Update the car document to include the selected profile
      const carDocRef = doc(firestore, "cars", car.id);
      await updateDoc(carDocRef, { selected_profile_id: profileId });

      // Navigate to the CarControlScreen with the selected car and profile IDs
      navigation.navigate("CarControl", { carId: car.id, profileId });
    } catch (error) {
      console.error("Error updating car document:", error);
    }
  };

  const onRefresh = () => {
    setRefreshing(true);
    fetchCars();
    setError(null);
    setRefreshing(false);
  };

  return (
    <ScrollView
      style={styles.container}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      {error && (
        <View style={styles.errorContainer}>
          <Text style={styles.errorText}>{error}</Text>
        </View>
      )}

      <View style={styles.header}>
        <Text style={styles.title}>Select Car</Text>
      </View>
      <View style={styles.carsContainer}>
        {cars.map((car, index) => (
          <TouchableOpacity
            key={index}
            style={styles.carButton}
            onPress={() => handleCarSelection(car)}
          >
            <Image
              source={{ uri: car.image_url }}
              style={styles.carImage}
              resizeMode="contain" // or "cover"
            />
            <Text style={styles.carButtonText}>
              {car.make} {car.model}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    paddingHorizontal: 20,
    paddingTop: 40,
  },
  header: {
    marginBottom: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#333333",
  },
  carsContainer: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  carButton: {
    width: "100%",
    backgroundColor: "#ffffff",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    marginBottom: 20,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  carImage: {
    width: "100%",
    aspectRatio: 3, // Adjust the aspect ratio as needed
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  carButtonText: {
    color: "#333333",
    fontSize: 16,
    fontWeight: "bold",
    paddingVertical: 10,
  },
  errorContainer: {
    backgroundColor: "red",
    padding: 10,
    marginBottom: 10,
    borderRadius: 5,
  },
  errorText: {
    color: "#fff",
    textAlign: "center",
  },
});

export default CarSelectionScreen;
