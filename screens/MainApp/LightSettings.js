import React, { useState, useEffect } from "react";
import { View, Text, Switch, StyleSheet } from "react-native";
import ColorPicker from "react-native-wheel-color-picker"; // Assumes you've installed a color picker library
import { Entypo } from "@expo/vector-icons";
import Slider from "@react-native-community/slider";

const LightSettings = ({ carSettings, onSettingChange }) => {
  // Initialize state variables with carSettings
  const [lightStatus, setLightStatus] = useState(
    carSettings?.ambient_light_status || false
  );
  const [brightness, setBrightness] = useState(
    carSettings?.ambient_light_brightness || 0
  );
  const [selectedColor, setSelectedColor] = useState(
    carSettings
      ? `#<span class="math-inline">\{carSettings\.ambient\_light\_color\_red\.toString\(16\)\.padStart\(2, '0'\)\}</span>{carSettings.ambient_light_color_green.toString(16).padStart(2, '0')}${carSettings.ambient_light_color_blue
          .toString(16)
          .padStart(2, "0")}`
      : "#000000"
  ); // Use hexadecimal format

  const [isModified, setIsModified] = useState(false);

  useEffect(() => {
    // Update parent component with initial settings
    onSettingChange("ambient_light_control", {
      ambient_light_status: lightStatus,
      ambient_light_brightness: brightness,
      ambient_light_color_red: parseInt(selectedColor.slice(1, 3), 16), // Extract and convert to number
      ambient_light_color_green: parseInt(selectedColor.slice(3, 5), 16),
      ambient_light_color_blue: parseInt(selectedColor.slice(5, 7), 16),
    });
  }, []);

  const handleSettingChange = (settingName, value) => {
    setIsModified(true);

    switch (settingName) {
      case "lightStatus":
        setLightStatus(value);
        break;
      case "brightness":
        setBrightness(Math.round(value)); // Round to integer
        break;
      case "selectedColor":
        setSelectedColor(value);
        break;
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.section}>
        <Text style={styles.label}>Ambient Light:</Text>
        <Switch
          value={lightStatus}
          onValueChange={(value) => handleSettingChange("lightStatus", value)}
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={lightStatus ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
        />
      </View>

      <View style={styles.section}>
        <Text style={styles.label}>Brightness:</Text>
        <Slider
          style={[styles.slider, isModified && styles.modifiedSlider]}
          minimumValue={0}
          maximumValue={100}
          value={brightness}
          step={1} // Ensure the brightness slider is an integer value (0-100%)
          onValueChange={(value) => handleSettingChange("brightness", value)}
        />
        <Text style={styles.valueText}>{Math.round(brightness)}%</Text>
      </View>

      <View style={[styles.section, styles.colorSection]}>
        <Text style={styles.label}>Color:</Text>
        <ColorPicker
          color={selectedColor}
          onColorChange={setSelectedColor}
          onColorChangeComplete={handleSettingChange.bind(
            null,
            "selectedColor"
          )}
          thumbSize={40}
          sliderHidden={true}
          noSnap={true}
          row={false}
          swatchesOnly={true}
          swatchesLast={true}
        />

        {/* RGB Values (below the color picker) */}
        <View style={styles.colorValueContainer}>
          <View style={styles.colorValueItem}>
            <Text style={styles.colorLabel}>R:</Text>
            <Text style={styles.colorNumber}>
              {parseInt(selectedColor.slice(1, 3), 16)}
            </Text>
          </View>
          <View style={styles.colorValueItem}>
            <Text style={styles.colorLabel}>G:</Text>
            <Text style={styles.colorNumber}>
              {parseInt(selectedColor.slice(3, 5), 16)}
            </Text>
          </View>
          <View style={styles.colorValueItem}>
            <Text style={styles.colorLabel}>B:</Text>
            <Text style={styles.colorNumber}>
              {parseInt(selectedColor.slice(5, 7), 16)}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  section: {
    marginBottom: 20,
  },
  colorSection: {
    marginBottom: 20,
    alignItems: "center",
  },
  label: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
  },
  colorPickerAndValue: {
    flexDirection: "column",
    alignItems: "center",
  },
  colorValueContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 50,
    width: 200,
  },
  colorValueItem: {
    alignItems: "center",
  },
  colorLabel: {
    fontSize: 16,
    fontWeight: "bold",
  },
  colorNumber: {
    fontSize: 18,
  },
});

export default LightSettings;
