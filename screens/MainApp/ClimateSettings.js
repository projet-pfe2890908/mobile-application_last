import React, { useState } from "react";
import { View, Text, Switch, StyleSheet } from "react-native";
import { Alert } from "react-native";
import Slider from "@react-native-community/slider";

const ClimateSettings = ({ carSettings, onSettingChange }) => {
  // Initialize state variables with carSettings or default values
  const [temperature, setTemperature] = useState(
    carSettings?.temperature_setting || 22.0
  );
  const [autoProgramActive, setAutoProgramActive] = useState(
    carSettings?.auto_program_active || false
  );
  const [maxCoolingActive, setMaxCoolingActive] = useState(
    carSettings?.maximum_cooling_active || false
  );
  const [coolingFunctionActive, setCoolingFunctionActive] = useState(
    carSettings?.cooling_function_active || false
  );
  const [rearWindowDefrosterActive, setRearWindowDefrosterActive] = useState(
    carSettings?.rear_window_defroster_active || false
  );
  const [defrostWindowsActive, setDefrostWindowsActive] = useState(
    carSettings?.defrost_windows_active || false
  );
  const [systemPowerStatus, setSystemPowerStatus] = useState(
    carSettings?.system_power_status || false
  );
  const [fanSpeed, setFanSpeed] = useState(carSettings?.fan_speed || 0);

  const [isModified, setIsModified] = useState(false);
  const handleSettingChange = (settingName, value) => {
    setIsModified(true);
    // Update local state immediately
    switch (settingName) {
      case "temperature":
        setTemperature(value);
        break;
      case "autoProgramActive":
        setAutoProgramActive(value);
        break;
      case "maxCoolingActive":
        setMaxCoolingActive(value);
        break;
      case "coolingFunctionActive":
        setCoolingFunctionActive(value);
        break;
      case "rearWindowDefrosterActive":
        setRearWindowDefrosterActive(value);
        break;
      case "defrostWindowsActive":
        setDefrostWindowsActive(value);
        break;
      case "systemPowerStatus":
        setSystemPowerStatus(value);
        break;
      case "fanSpeed":
        setFanSpeed(value);
        break;
    }

    // Call the callback function to update the parent component
    onSettingChange("climate_control", {
      temperature_setting: temperature,
      auto_program_active: autoProgramActive,
      maximum_cooling_active: maxCoolingActive,
      cooling_function_active: coolingFunctionActive,
      rear_window_defroster_active: rearWindowDefrosterActive,
      defrost_windows_active: defrostWindowsActive,
      system_power_status: systemPowerStatus,
      fan_speed: fanSpeed,
    });
  };

  return (
    <View style={styles.container}>
      {/* ... (Temperature and Fan Speed Sliders) ... */}
      <Text style={styles.label}>Temperature (°C):</Text>
      <Slider
        style={[styles.slider, isModified && styles.modifiedSlider]}
        minimumValue={16.0}
        maximumValue={32.0}
        step={0.5} // Allow for half-degree increments
        value={temperature}
        onValueChange={(value) => handleSettingChange("temperature", value)}
      />
      <Text style={styles.valueText}>{temperature.toFixed(1)}°C</Text>

      <Text style={styles.label}>Fan Speed:</Text>
      <Slider
        style={[styles.slider, isModified && styles.modifiedSlider]}
        minimumValue={0}
        maximumValue={5} // Adjust as per car's fan speed settings
        step={1}
        value={fanSpeed}
        onValueChange={(value) => handleSettingChange("fanSpeed", value)}
      />
      <Text style={styles.valueText}>{fanSpeed}</Text>
      {/* Switches for Other Climate Settings */}
      <View style={styles.switchContainer}>
        <Text style={styles.label}>Auto:</Text>
        <Switch
          value={autoProgramActive}
          onValueChange={(value) =>
            handleSettingChange("autoProgramActive", value)
          }
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={autoProgramActive ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
        />
      </View>

      {/* Additional Switches */}
      <View style={styles.switchContainer}>
        <Text style={styles.label}>Max Cooling:</Text>
        <Switch
          value={maxCoolingActive}
          onValueChange={(value) =>
            handleSettingChange("maxCoolingActive", value)
          }
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={maxCoolingActive ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
        />
      </View>
      <View style={styles.switchContainer}>
        <Text style={styles.label}>AC:</Text>
        <Switch
          value={coolingFunctionActive}
          onValueChange={(value) =>
            handleSettingChange("coolingFunctionActive", value)
          }
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={coolingFunctionActive ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
        />
      </View>
      <View style={styles.switchContainer}>
        <Text style={styles.label}>Rear Defrost:</Text>
        <Switch
          value={rearWindowDefrosterActive}
          onValueChange={(value) =>
            handleSettingChange("rearWindowDefrosterActive", value)
          }
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={rearWindowDefrosterActive ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
        />
      </View>
      <View style={styles.switchContainer}>
        <Text style={styles.label}>Defrost:</Text>
        <Switch
          value={defrostWindowsActive}
          onValueChange={(value) =>
            handleSettingChange("defrostWindowsActive", value)
          }
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={defrostWindowsActive ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
        />
      </View>
      <View style={styles.switchContainer}>
        <Text style={styles.label}>System Power:</Text>
        <Switch
          value={systemPowerStatus}
          onValueChange={(value) =>
            handleSettingChange("systemPowerStatus", value)
          }
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={systemPowerStatus ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  slider: {
    width: "100%",
    marginBottom: 15,
  },
  modifiedSlider: {
    thumbTintColor: "orange", // Change thumb color when modified
  },
  valueText: {
    textAlign: "center",
    marginBottom: 10,
    fontSize: 14, // slightly smaller font size for the value
  },
  switchContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
});

export default ClimateSettings;
