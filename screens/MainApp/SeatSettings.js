import React, { useState } from "react";
import { View, Text, Switch, StyleSheet } from "react-native";
import Slider from "@react-native-community/slider";

const SeatSettings = ({ carSettings, onSettingChange }) => {
  // Initialize state variables with carSettings
  const [horizontalPosition, setHorizontalPosition] = useState(
    carSettings?.forward_backward_adjustment || 0
  );
  const [reclineAngle, setReclineAngle] = useState(
    carSettings?.recline_angle_adjustment || 0
  );
  const [heightAdjustment, setHeightAdjustment] = useState(
    carSettings?.height_adjustment || 0
  );
  const [seatHeating, setSeatHeating] = useState(
    carSettings?.seat_heating || false
  );
  const [seatVentilation, setSeatVentilation] = useState(
    carSettings?.seat_ventilation || false
  );
  const [isModified, setIsModified] = useState(false);

  const handleSettingChange = (settingName, value) => {
    setIsModified(true);

    // Update local state immediately
    switch (settingName) {
      case "horizontalPosition":
        setHorizontalPosition(value);
        break;
      case "reclineAngle":
        setReclineAngle(value);
        break;
      case "heightAdjustment":
        setHeightAdjustment(value);
        break;
      case "seatHeating":
        setSeatHeating(value);
        break;
      case "seatVentilation":
        setSeatVentilation(value);
        break;
    }

    // Call the callback function to update the parent component
    onSettingChange("seat_controls", {
      forward_backward_adjustment: horizontalPosition,
      recline_angle_adjustment: reclineAngle,
      height_adjustment: heightAdjustment,
      seat_heating: seatHeating,
      seat_ventilation: seatVentilation,
    });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Horizontal Position (cm):</Text>
      <Slider
        style={[styles.slider, isModified && styles.modifiedSlider]}
        minimumValue={-10}
        maximumValue={10}
        step={1}
        value={horizontalPosition}
        onValueChange={(value) =>
          handleSettingChange("horizontalPosition", value)
        }
      />
      <Text style={styles.valueText}>{horizontalPosition} cm</Text>

      <Text style={styles.label}>Recline Angle (degrees):</Text>
      <Slider
        style={[styles.slider, isModified && styles.modifiedSlider]}
        minimumValue={-15}
        maximumValue={15}
        step={1}
        value={reclineAngle}
        onValueChange={(value) => handleSettingChange("reclineAngle", value)}
      />
      <Text style={styles.valueText}>{reclineAngle}°</Text>

      <Text style={styles.label}>Height Adjustment (cm):</Text>
      <Slider
        style={[styles.slider, isModified && styles.modifiedSlider]}
        minimumValue={-5}
        maximumValue={5}
        step={1}
        value={heightAdjustment}
        onValueChange={(value) =>
          handleSettingChange("heightAdjustment", value)
        }
      />
      <Text style={styles.valueText}>{heightAdjustment} cm</Text>

      <View style={styles.switchContainer}>
        <Text style={styles.label}>Seat Heating:</Text>
        <Switch
          value={seatHeating}
          onValueChange={(value) => handleSettingChange("seatHeating", value)}
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={seatHeating ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
        />
      </View>

      <View style={styles.switchContainer}>
        <Text style={styles.label}>Seat Ventilation:</Text>
        <Switch
          value={seatVentilation}
          onValueChange={(value) =>
            handleSettingChange("seatVentilation", value)
          }
          trackColor={{ false: "#767577", true: "#81b0ff" }}
          thumbColor={seatVentilation ? "#f5dd4b" : "#f4f3f4"}
          ios_backgroundColor="#3e3e3e"
        />
      </View>
    </View>
  );
};

// Styles (styles.js)
const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  slider: {
    width: "100%",
    marginBottom: 15,
  },
  modifiedSlider: {
    thumbTintColor: "orange",
  },
  valueText: {
    textAlign: "center",
    marginBottom: 10,
    fontSize: 14,
  },
  switchContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
});
export default SeatSettings;
