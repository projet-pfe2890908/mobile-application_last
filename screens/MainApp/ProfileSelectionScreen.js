import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Image,
  ScrollView,
  RefreshControl,
} from "react-native";
import {
  doc,
  collection,
  getDocs,
  query,
  where,
  updateDoc,
} from "firebase/firestore";
import { auth, firestore } from "../../firebaseConfig";
import { useFocusEffect } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import AvatarBoy from "../../assets/avatar/avatarBoy.png";

const ProfileSelectionScreen = ({ navigation }) => {
  const [profiles, setProfiles] = useState([]);
  const [selectedProfile, setSelectedProfile] = useState(null);
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    fetchProfiles();
    const clearFormAndState = () => {
      setPassword("");
      setError("");
      setSelectedProfile(null);
    };

    const onScreenFocus = navigation.addListener("focus", clearFormAndState);

    return () => {
      onScreenFocus();
    };
  }, []);

  const fetchProfiles = async () => {
    try {
      setLoading(true);
      const userDocRef = doc(firestore, "users", auth.currentUser.uid);
      const profilesCollectionRef = collection(userDocRef, "profiles");
      const profilesQuerySnapshot = await getDocs(profilesCollectionRef);
      const profilesData = profilesQuerySnapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));
      setProfiles(profilesData);
    } catch (error) {
      console.error("Error fetching profiles:", error);
      setError("Failed to fetch profiles. Please try again.");
    } finally {
      setLoading(false);
    }
  };

  // Fetch profiles when the screen comes into focus
  useFocusEffect(
    React.useCallback(() => {
      fetchProfiles();
    }, [])
  );

  const handleProfileSelection = (profile) => {
    if (selectedProfile && selectedProfile.id === profile.id) {
      // If the same profile is clicked again, deselect it
      setSelectedProfile(null);
    } else {
      // Otherwise, select the profile
      setSelectedProfile(profile);
    }
    // Reset error message
    setError("");
  };

  const handleProceed = async () => {
    if (!selectedProfile) {
      setError("Please select a profile.");
      return;
    }

    const userPassword = selectedProfile.password;

    if (password !== userPassword) {
      setError("Incorrect password. Please try again.");
      return;
    }

    // Update is_active field in the selected profile document
    const profileDocRef = doc(
      firestore,
      "users",
      auth.currentUser.uid,
      "profiles",
      selectedProfile.id
    );
    await updateDoc(profileDocRef, { is_active: true });

    // Update selected_profile_id field in the user document
    const userDocRef = doc(firestore, "users", auth.currentUser.uid);
    await updateDoc(userDocRef, { selected_profile_id: selectedProfile.id });

    navigation.navigate("CarSelection", { profileId: selectedProfile.id });
  };

  const onRefresh = () => {
    setRefreshing(true);
    setPassword("");
    setError("");
    setSelectedProfile(null);
    setRefreshing(false);
  };

  return (
    <ScrollView
      style={styles.container}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      <View style={styles.header}>
        <Text style={styles.title}>Select Profile</Text>
        <TouchableOpacity
          style={styles.settingsIcon}
          onPress={() => navigation.navigate("Settings")}
        >
          <Ionicons name="settings-outline" size={24} color="#000000" />
        </TouchableOpacity>
      </View>
      <View style={styles.profilesContainer}>
        {profiles.map((profile, index) => (
          <TouchableOpacity
            key={index}
            style={[
              styles.profileButton,
              selectedProfile === profile && styles.selectedProfileButton,
            ]}
            onPress={() => handleProfileSelection(profile)}
          >
            <Image source={AvatarBoy} style={styles.avatar} />
            <Text style={styles.profileButtonText}>{profile.firstName}</Text>
          </TouchableOpacity>
        ))}
      </View>
      {selectedProfile && (
        <View style={styles.passwordContainer}>
          <TextInput
            style={styles.input}
            placeholder="Enter Password"
            onChangeText={setPassword}
            value={password}
            secureTextEntry={true}
          />
          <TouchableOpacity style={styles.button} onPress={handleProceed}>
            <Text style={styles.buttonText}>Proceed</Text>
          </TouchableOpacity>
          {error !== "" && <Text style={styles.errorText}>{error}</Text>}
        </View>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    paddingHorizontal: 20,
    paddingTop: 40,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#333333",
  },
  settingsIcon: {
    padding: 10,
  },
  profilesContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
    marginBottom: 20,
  },
  profileButton: {
    alignItems: "center",
    width: 120,
    margin: 10,
    backgroundColor: "#007BFF",
    borderRadius: 10,
    padding: 10,
  },
  selectedProfileButton: {
    backgroundColor: "#0056B3",
  },
  profileButtonText: {
    color: "#FFFFFF",
    fontSize: 16,
    fontWeight: "bold",
    marginTop: 5,
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  passwordContainer: {
    alignItems: "center",
  },
  input: {
    width: "100%",
    height: 50,
    borderColor: "#CCCCCC",
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 15,
    fontSize: 16,
    backgroundColor: "#FFFFFF",
  },
  button: {
    width: "100%",
    height: 50,
    backgroundColor: "#28A745",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
    marginBottom: 20,
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 18,
    fontWeight: "bold",
  },
  errorText: {
    color: "#FF0000",
    fontSize: 16,
    marginBottom: 10,
  },
});

export default ProfileSelectionScreen;
