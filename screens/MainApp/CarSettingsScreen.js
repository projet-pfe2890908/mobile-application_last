import React, { useState } from "react";
import { View, Text, TouchableOpacity, StyleSheet, Alert } from "react-native";
import SeatSettings from "./SeatSettings";
import LightSettings from "./LightSettings";
import ClimateSettings from "./ClimateSettings";

const CarSettingsScreen = ({ route, navigation }) => {
  const { carId, settingType } = route.params;
  const [carSettings, setCarSettings] = useState({
    settings: {
      // Default car settings for demonstration purposes
      seat_controls: {
        forward_backward_adjustment: 0,
        recline_angle_adjustment: 0,
        height_adjustment: 0,
        seat_heating: false,
        seat_ventilation: false,
      },
      ambient_light_control: {
        ambient_light_status: false,
        ambient_light_color_red: 0,
        ambient_light_color_green: 0,
        ambient_light_color_blue: 0,
        ambient_light_brightness: 0,
      },
      climate_control: {
        temperature_setting: 22,
        auto_program_active: false,
        maximum_cooling_active: false,
        cooling_function_active: false,
        rear_window_defroster_active: false,
        defrost_windows_active: false,
        system_power_status: false,
        fan_speed: 0,
      },
    },
  });
  const [isModified, setIsModified] = useState(false);

  const handleSettingChange = (settingType, updatedSettings) => {
    setIsModified(true);
    setCarSettings((prevSettings) => ({
      ...prevSettings,
      settings: {
        ...prevSettings.settings,
        [settingType + "_controls"]: updatedSettings,
      },
    }));
  };

  const handleSaveSettings = async () => {
    // Placeholder for saving settings - you'll implement Firebase logic later
    console.log("Car Settings to be saved:", carSettings.settings);
    Alert.alert("Success", "Settings Saved Successfully"); // Show a success message
    setIsModified(false);
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Car Settings - {settingType}</Text>

      {settingType === "seat" && !carSettings.settings.seat_controls && (
        <Text style={styles.noSettingsText}>
          No seat settings available yet.
        </Text>
      )}

      {settingType === "seat" && carSettings.settings.seat_controls && (
        <SeatSettings
          carSettings={carSettings.settings.seat_controls}
          onSettingChange={handleSettingChange}
        />
      )}
      {settingType === "light" && (
        <LightSettings
          carSettings={carSettings.settings.ambient_light_control}
          onSettingChange={handleSettingChange}
        />
      )}
      {settingType === "climate" && (
        <ClimateSettings
          carSettings={carSettings.settings.climate_control}
          onSettingChange={handleSettingChange}
        />
      )}
      <TouchableOpacity style={styles.saveButton} onPress={handleSaveSettings}>
        <Text style={styles.saveButtonText}>Save Settings</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: "#fff", // White background
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
    color: "#333", // Dark grey text
    textAlign: "center",
  },
  saveButton: {
    backgroundColor: "#007bff", // Blue button
    padding: 15,
    borderRadius: 8,
    alignItems: "center",
    marginTop: 20,
  },
  saveButtonText: {
    color: "#fff", // White text
    fontWeight: "bold",
    fontSize: 18,
  },
  loadingIndicator: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  errorText: {
    color: "red",
    fontSize: 16,
    textAlign: "center",
  },
  saveButtonContainer: {
    marginTop: 20, // Add margin to separate from content
    alignItems: "center", // Center the button horizontally
  },
});

export default CarSettingsScreen;
