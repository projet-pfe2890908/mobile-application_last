import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons'; // Import MaterialIcons from Expo vector icons
import { doc, updateDoc, collection, query, where, getDocs, arrayUnion, arrayContains, getDoc } from 'firebase/firestore';
import { auth, firestore } from '../../firebaseConfig';

const CarLinkingScreen = ({ navigation }) => {
  const [carId, setCarId] = useState('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleLinkCar = async () => {
    try {
      setError(null);
      setLoading(true);

      // Validate car ID
      if (!carId.trim()) {
        setError('Please enter a valid car ID.');
        return;
      }

      // Check if the car exists in Firestore
      const carsCollectionRef = collection(firestore, 'cars');
      const carsQuery = query(carsCollectionRef, where('car_id', '==', carId));
      const carQuerySnapshot = await getDocs(carsQuery);

      if (carQuerySnapshot.empty) {
        setError('Car not found. Please check the car ID.');
        return;
      }

      // Get the document reference of the first car found
      const carDoc = carQuerySnapshot.docs[0]; // Get the first car document
      const carDocRef = doc(firestore, 'cars', carDoc.id); // Get the document reference

      // Check if the car is already linked to another user
      if (carDoc.data().owner_id && carDoc.data().owner_id !== auth.currentUser.uid) {
        setError('Car is already linked to another account.');
        return;
      }

      // Check if the key is inside the car
      const carData = carDoc.data();
      if (!carData.key_status) {
        setError('Please insert the key into the car to proceed with linking.');
        return;
      }

      // Fetch linked cars for the current user
      const userDocRef = doc(firestore, 'users', auth.currentUser.uid);
      const userDocSnapshot = await getDoc(userDocRef);
      let linkedCars = userDocSnapshot.data().linked_cars || [];

      // Create linked_cars field if it doesn't exist
      if (!linkedCars) {
        linkedCars = [];
        await updateDoc(userDocRef, { linked_cars: linkedCars });
      }

      // Update the owner_id field in the car document
      await updateDoc(carDocRef, { owner_id: auth.currentUser.uid });

      // Update the user document with linked car document ID
      await updateDoc(userDocRef, { linked_cars: arrayUnion(carDoc.id) });

      // Check if the setup is completed
      const userData = userDocSnapshot.data();
      navigation.navigate(userData.setupCompleted ? 'ProfileSelection' : 'ProfileCreation');
    } catch (error) {
      console.error('Error linking car:', error);
      setError('Failed to link car. Please try again.');
    } finally {
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Link Your Car</Text>
      {error && <Text style={styles.errorText}>{error}</Text>}
      <View style={styles.inputContainer}>
        <MaterialIcons name="directions-car" size={24} color="#007bff" />
        <TextInput
          style={styles.input}
          placeholder="Enter Car ID"
          onChangeText={setCarId}
          value={carId}
          keyboardType="default"
          autoCapitalize="none"
          editable={!loading}
        />
      </View>
      <TouchableOpacity
        style={[styles.button, loading && styles.disabledButton]}
        onPress={handleLinkCar}
        disabled={loading}
      >
        <Text style={styles.buttonText}>{loading ? 'Linking Car...' : 'Link Car'}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
    fontWeight: 'bold',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: 50,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 15,
    backgroundColor: '#ffffff',
  },
  input: {
    flex: 1,
    fontSize: 16,
  },
  button: {
    width: '100%',
    height: 50,
    backgroundColor: '#007bff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 20,
  },
  disabledButton: {
    backgroundColor: '#ccc',
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  errorText: {
    color: 'red',
    fontSize: 16,
    marginBottom: 10,
  },
});

export default CarLinkingScreen;
