import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

const SetupScreen = ({ navigation }) => {
  const handleNavigateToCarLinking = () => {
    navigation.navigate('CarLinking');
  };

  return (
    <View style={styles.container}>
      <MaterialIcons name="directions-car" size={64} color="#007bff" />
      <Text style={styles.title}>Welcome to Smart Car Connect!</Text>
      <Text style={styles.description}>
        Let's get started by setting up your app.
      </Text>
      <TouchableOpacity
        onPress={handleNavigateToCarLinking}
        style={styles.button}
      >
        <Text style={styles.buttonText}>Link Your Car</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#ffffff',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    textAlign: 'center',
  },
  description: {
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 40,
    color: '#666',
  },
  button: {
    backgroundColor: '#007bff',
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 8,
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default SetupScreen;
