import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  RefreshControl,
  Modal,
} from "react-native";
import { doc, collection, addDoc, getDoc } from "firebase/firestore";
import { auth, firestore } from "../../firebaseConfig";
import { MaterialIcons } from "@expo/vector-icons"; // Import MaterialIcons from Expo vector icons
import DropDownPicker from "react-native-dropdown-picker"; // Import DropDownPicker component
import Icon from "react-native-vector-icons/FontAwesome";

const ProfileCreationScreen = ({ navigation }) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [gender, setGender] = useState(null); // Changed to null initially
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [isGenderPickerOpen, setIsGenderPickerOpen] = useState(false);

  const handleCreateProfile = async () => {
    try {
      setError(null);
      setLoading(true);

      // Validate profile data
      if (
        !firstName.trim() ||
        !lastName.trim() ||
        !gender.trim() ||
        !password.trim() ||
        !confirmPassword.trim()
      ) {
        setError("Please fill in all fields.");
        setLoading(false); // Reset loading state
        return;
      }

      if (password !== confirmPassword) {
        setError("Passwords do not match.");
        setLoading(false); // Reset loading state
        return;
      }

      // Create profile document
      const profileData = {
        firstName,
        lastName,
        gender,
        password,
        is_active: false,
        presets: [], // Initialize an empty presets array
      };

      const userDocRef = doc(firestore, "users", auth.currentUser.uid);
      const profileDocRef = await addDoc(
        collection(userDocRef, "profiles"),
        profileData
      );

      // Fetch user data to check if setup is completed
      const userDocSnapshot = await getDoc(userDocRef);
      const userData = userDocSnapshot.data();

      // Check if the setup is completed
      if (userData.setupCompleted) {
        navigation.navigate("ProfileSelection");
      } else {
        navigation.navigate("SetupCompleted");
      }
    } catch (error) {
      console.error("Error creating profile:", error);
      setError("Failed to create profile. Please try again.");
    } finally {
      setLoading(false); // Reset loading state in case of any error
    }
  };

  const onRefresh = () => {
    // Reset form fields and state
    setFirstName("");
    setLastName("");
    setGender(null); // Reset gender to null
    setPassword("");
    setConfirmPassword("");
    setError(null);
    setLoading(false);
    setShowPassword(false);
    setShowConfirmPassword(false);
    setRefreshing(false);
  };

  return (
    <ScrollView
      contentContainerStyle={styles.container}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }
    >
      <View style={styles.content}>
        <Text style={styles.title}>Create Profile</Text>
        {error && <Text style={styles.errorText}>{error}</Text>}
        <TextInput
          style={styles.input}
          placeholder="First Name"
          onChangeText={setFirstName}
          value={firstName}
          autoCapitalize="words"
        />
        <TextInput
          style={styles.input}
          placeholder="Last Name"
          onChangeText={setLastName}
          value={lastName}
          autoCapitalize="words"
        />
        <TouchableOpacity
          style={styles.genderPicker}
          onPress={() => setIsGenderPickerOpen(true)}
        >
          <Text style={styles.genderPickerText}>
            {gender ? gender : "Select Gender"}
          </Text>
          <Icon name="caret-down" size={20} color="#777" style={styles.icon} />
        </TouchableOpacity>
        <Modal
          visible={isGenderPickerOpen}
          transparent={true}
          animationType="slide"
          onRequestClose={() => setIsGenderPickerOpen(false)}
        >
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <Text style={styles.modalTitle}>Select Gender</Text>
              <DropDownPicker
                open={isGenderPickerOpen}
                value={gender}
                items={[
                  { label: "Male", value: "male" },
                  { label: "Female", value: "female" },
                ]}
                setOpen={setIsGenderPickerOpen}
                setValue={setGender}
                onClose={() => setIsGenderPickerOpen(false)}
                containerStyle={styles.dropDownContainer}
                style={styles.dropDownPicker}
                labelStyle={styles.dropDownLabel}
                selectedItemLabelStyle={styles.dropDownSelectedItemLabel}
                arrowIconContainerStyle={styles.dropDownArrowIcon}
                zIndex={5000}
              />
            </View>
          </View>
        </Modal>

        <View style={styles.passwordContainer}>
          <TextInput
            style={styles.passwordInput}
            placeholder="Password"
            onChangeText={setPassword}
            value={password}
            secureTextEntry={!showPassword}
          />
          <TouchableOpacity
            style={styles.toggleButton}
            onPress={() => setShowPassword(!showPassword)}
          >
            <MaterialIcons
              name={showPassword ? "visibility-off" : "visibility"}
              size={24}
              color="#777"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.passwordContainer}>
          <TextInput
            style={styles.passwordInput}
            placeholder="Confirm Password"
            onChangeText={setConfirmPassword}
            value={confirmPassword}
            secureTextEntry={!showConfirmPassword}
          />
          <TouchableOpacity
            style={styles.toggleButton}
            onPress={() => setShowConfirmPassword(!showConfirmPassword)}
          >
            <MaterialIcons
              name={showConfirmPassword ? "visibility-off" : "visibility"}
              size={24}
              color="#777"
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={[styles.button, loading && styles.disabledButton]}
          onPress={handleCreateProfile}
          disabled={loading}
        >
          <Text style={styles.buttonText}>
            {loading ? "Creating Profile..." : "Create Profile"}
          </Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ffffff",
  },
  content: {
    width: "80%",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 20,
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
    fontWeight: "bold",
  },
  input: {
    width: "100%",
    height: 50,
    borderColor: "#ccc",
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 15,
    fontSize: 16,
  },
  icon: {
    marginRight: 10,
  },
  genderPicker: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    height: 50,
    borderColor: "#ccc",
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 15,
    backgroundColor: "#ffffff",
  },
  genderPickerText: {
    fontSize: 16,
    color: "#333", // Darken the text color for better readability
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    paddingHorizontal: 20, // Add horizontal padding for modal container
  },
  modalContent: {
    backgroundColor: "#ffffff",
    padding: 20,
    borderRadius: 10,
    elevation: 5,
    maxHeight: "80%",
    maxWidth: "80%",
  },
  modalTitle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
    textAlign: "center",
    color: "#333", // Darken the text color for better readability
  },
  dropDownContainer: {
    marginTop: 20,
  },
  dropDownPicker: {
    backgroundColor: "#ffffff",
  },
  dropDownLabel: {
    fontSize: 16,
    color: "#333", // Darken the text color for better readability
  },
  dropDownSelectedItemLabel: {
    color: "#007bff",
  },
  dropDownArrowIcon: {
    justifyContent: "center",
  },
  passwordContainer: {
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    height: 50,
    borderColor: "#ccc",
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 20,
    paddingHorizontal: 15,
    backgroundColor: "#ffffff",
  },
  passwordInput: {
    flex: 1,
    fontSize: 16,
  },
  toggleButton: {
    padding: 10,
  },
  button: {
    width: "100%",
    height: 50,
    backgroundColor: "#007bff",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5,
  },
  disabledButton: {
    backgroundColor: "#ccc",
  },
  buttonText: {
    color: "#ffffff",
    fontSize: 18,
    fontWeight: "bold",
  },
  errorText: {
    color: "red",
    fontSize: 16,
    marginBottom: 10,
  },
});

export default ProfileCreationScreen;
