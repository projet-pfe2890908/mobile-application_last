import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { doc, updateDoc } from 'firebase/firestore';
import { auth, firestore } from '../../firebaseConfig';

const SetupCompletedScreen = ({ navigation }) => {
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const markSetupCompleted = async () => {
            try {
                // Update Firestore to mark setup as completed
                const userDocRef = doc(firestore, 'users', auth.currentUser.uid);
                await updateDoc(userDocRef, {
                    setupCompleted: true
                });

                // Simulate loading for 3 seconds
                setTimeout(() => {
                    setLoading(false);
                    // Navigate to ProfileSelectionScreen after 3 seconds
                    navigation.navigate('ProfileSelection');
                }, 3000);
            } catch (error) {
                console.error('Error marking setup as completed:', error);
            }
        };

        markSetupCompleted();

        // Clean-up function for useEffect
        return () => {
            clearTimeout(timer); // Clear the timer if the component unmounts before the delay
        };
    }, [navigation]);

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Setup Completed</Text>
            {loading && (
                <ActivityIndicator size="large" color="#007bff" style={styles.activityIndicator} />
            )}
            <Text style={styles.description}>
                {!loading && 'You have successfully completed the setup process.'}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
        backgroundColor: '#ffffff',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        textAlign: 'center',
    },
    description: {
        fontSize: 18,
        textAlign: 'center',
        marginBottom: 40,
        color: '#666',
    },
    activityIndicator: {
        marginTop: 20,
    },
});

export default SetupCompletedScreen;
