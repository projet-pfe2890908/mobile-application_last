import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import LoginScreen from "./screens/Authentification/LoginScreen";
import RegisterScreen from "./screens/Authentification/RegisterScreen";
import VerifyEmailScreen from "./screens/Authentification/VerifyEmailScreen";
import ForgotPasswordScreen from "./screens/Authentification/ForgotPasswordScreen";

import SetupScreen from "./screens/Setup/SetupScreen";
import CarLinkingScreen from "./screens/Setup/CarLinkingScreen ";
import ProfileCreationScreen from "./screens/Setup/ProfileCreationScreen";
import SetupCompletedScreen from "./screens/Setup/SetupCompletedScreen";

import SettingsScreen from "./screens/SettingsMenu/SettingsScreen";
import ManageProfilesScreen from "./screens/SettingsMenu/ManageProfilesScreen";
import DeleteLinkScreen from "./screens/SettingsMenu/DeleteLinkScreen";

import ProfileSelectionScreen from "./screens/MainApp/ProfileSelectionScreen";
import CarControlScreen from "./screens/MainApp/CarControlScreen";
import CarSelectionScreen from "./screens/MainApp/CarSelectionScreen ";
import PresetCreationScreen from "./screens/MainApp/PresetCreationScreen ";
import CarSettingsScreen from "./screens/MainApp/CarSettingsScreen";
import SeatSettings from "./screens/MainApp/SeatSettings";
import LightSettings from "./screens/MainApp/LightSettings";
import ClimateSettings from "./screens/MainApp/ClimateSettings";

const StackNavigator = () => {
  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen name="Setup" component={SetupScreen} />
        <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} />
        <Stack.Screen name="VerifyEmail" component={VerifyEmailScreen} />
        <Stack.Screen name="CarLinking" component={CarLinkingScreen} />
        <Stack.Screen
          name="ProfileCreation"
          component={ProfileCreationScreen}
        />
        <Stack.Screen name="SetupCompleted" component={SetupCompletedScreen} />
        <Stack.Screen
          name="ProfileSelection"
          component={ProfileSelectionScreen}
        />
        <Stack.Screen name="CarControl" component={CarControlScreen} />
        <Stack.Screen name="Settings" component={SettingsScreen} />
        <Stack.Screen name="ManageProfiles" component={ManageProfilesScreen} />
        <Stack.Screen name="DeleteLink" component={DeleteLinkScreen} />
        <Stack.Screen name="CarSelection" component={CarSelectionScreen} />
        <Stack.Screen name="PresetCreation" component={PresetCreationScreen} />
        <Stack.Screen name="CarSettings" component={CarSettingsScreen} />
        <Stack.Screen name="SeatSettings" component={SeatSettings} />
        <Stack.Screen name="LightSettings" component={LightSettings} />
        <Stack.Screen name="ClimateSettings" component={ClimateSettings} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default StackNavigator;

const styles = StyleSheet.create({});
